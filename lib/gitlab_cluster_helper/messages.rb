class GitlabClusterHelper
  module Messages
    CREATE = "Create new cluster and show credentials"
    QUIT = "Quit application and clean up any clusters that were created"
    GOODBYE = "Goodbye!"
  end
end
