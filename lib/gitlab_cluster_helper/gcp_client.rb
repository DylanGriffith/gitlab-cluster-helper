require 'json'
require 'base64'

class GitlabClusterHelper
  class GcpClient
    def initialize(config: GitlabClusterHelper::Config.instance, shell: GitlabClusterHelper::Shell.new)
      @config = config
      @shell = shell
    end

    def create_cluster
      cluster_name = "#{@config.cluster_prefix}-#{Time.now.strftime("%Y-%m-%dt%Hh%Mm%Ss")}"
      @shell.run([*gcloud, 'container', 'clusters', 'create', '--zone', @config.gcp_zone, cluster_name, '--enable-basic-auth', '--disk-size', '10GB'])

      @shell.run([*gcloud, 'container', 'clusters', 'get-credentials', '--zone', @config.gcp_zone, cluster_name])

      api_url = @shell.run(['kubectl', 'config', 'view', '--minify', '-o', "jsonpath='{.clusters[].cluster.server}'"])

      auth_json = @shell.run([*gcloud, 'container', 'clusters', 'describe', '--zone', @config.gcp_zone, cluster_name, '--format', "json(masterAuth.username, masterAuth.password)"])

      auth_data = JSON.parse(auth_json)
      username = auth_data['masterAuth']['username']
      password = auth_data['masterAuth']['password']

      @shell.run(['kubectl', 'config', 'set-credentials', cluster_name, '--username', username, '--password', password])

      @shell.run(['kubectl', 'create', 'serviceaccount', 'gitlab'])

      @shell.run([
        'kubectl', '--user', username,
        'create', 'clusterrolebinding',
        '--serviceaccount=default:gitlab', '--clusterrole=cluster-admin', 'gitlab-admin'
      ])

      secrets_json = @shell.run(['kubectl', 'get', 'secrets', '-o', 'json'])
      secrets_data = JSON.parse(secrets_json)
      gitlab_account = secrets_data['items'].find do |item|
        item['metadata']['annotations']['kubernetes.io/service-account.name'] == 'gitlab'
      end

      ca_certificate = Base64.decode64(gitlab_account['data']['ca.crt'])
      token = Base64.decode64(gitlab_account['data']['token'])

      Cluster.new(
        name: cluster_name,
        api_url: api_url,
        ca_certificate: ca_certificate,
        token: token
      )
    end

    def delete_cluster(cluster_name)
      @shell.run([*gcloud, 'container', 'clusters', 'delete', '--zone', @config.gcp_zone, cluster_name, '--quiet', '--async'])
    end

    private

    def gcloud
      ['gcloud', '--project', @config.gcp_project]
    end
  end
end
