class GitlabClusterHelper
  class Cluster
    attr_reader :name, :api_url, :ca_certificate, :token

    def initialize(name:,api_url:,ca_certificate:,token:)
      @name = name
      @api_url = api_url
      @ca_certificate = ca_certificate
      @token = token
    end
  end
end
