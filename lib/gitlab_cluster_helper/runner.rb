require 'readline'

class GitlabClusterHelper
  class Runner
    def initialize(manager: GitlabClusterHelper::Manager.new, readline_class: Readline, output: $stdout)
      @manager = manager
      @readline_class = readline_class
      @output = output
    end

    def start
      print_help

      while line = @readline_class.readline('> ', true)
        if command = commands[line]
          command[:command].call
        end

        @output.puts Messages::GOODBYE && break if line == 'q'

        print_help
      end
    end

    private

    def commands
      {
        'c' => { message: Messages::CREATE, command: method(:create) },
        'q' => { message: Messages::QUIT, command: @manager.method(:cleanup) },
      }
    end

    def create
      cluster = @manager.create

      @output.puts <<~END
      Name: #{cluster.name}
      Api URL: #{cluster.api_url}
      Ca Certificate:
      #{cluster.ca_certificate}
      Token: #{cluster.token}
      END
    end

    def print_help
      commands.each do |key, command|
        @output.puts "#{key}) #{command[:message]}"
      end
    end
  end
end
