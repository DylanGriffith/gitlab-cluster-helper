require 'open3'

class GitlabClusterHelper
  class Shell
    class Error < StandardError; end

    def run(command)
      puts "Running command: #{command.join(" ")}"
      stdin, stdout, stderr, wait_thr = Open3.popen3(*command)

      stdin.close

      out = stdout.read
      stdout.close

      err = stderr.read
      stderr.close

      exit_status = wait_thr.value

      raise Error, out + err unless exit_status.success?

      out.chomp
    end
  end
end
