class GitlabClusterHelper
  class Manager
    def initialize(gcp_client: GitlabClusterHelper::GcpClient.new)
      @gcp_client = gcp_client
      @clusters = []
      @next_cluster = nil
      create_next_cluster
    end

    def create
      @next_cluster_thread.join
      cluster = @next_cluster
      @next_cluster = nil
      @clusters << cluster

      create_next_cluster
      cluster
    end

    def cleanup
      @clusters.each do |cluster|
        @gcp_client.delete_cluster(cluster.name)
      end

      @next_cluster_thread.join

      @gcp_client.delete_cluster(@next_cluster.name)
    end

    def create_next_cluster
      @next_cluster_thread = Thread.new {
        next_cluster = @gcp_client.create_cluster

        @next_cluster = next_cluster
      }
    end
  end
end
