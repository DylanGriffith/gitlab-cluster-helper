require 'singleton'
require 'pathname'

class GitlabClusterHelper
  class Config
    include Singleton

    class Error < StandardError; end

    attr_reader :gcp_project, :gcp_zone, :cluster_prefix

    DEFAULT_PATH = Pathname.new('~').join('.gitlab-cluster-helper-config.json').expand_path

    def initialize
      path = ENV["GITLAB_CLUSTER_HELPER_CONFIG"] || DEFAULT_PATH

      raise Error, "Config file does not exist at path: #{path}" unless Pathname.new(path).file?

      json = File.read(path)
      data = JSON.parse(json)

      @gcp_project = data['gcp_project']
      @gcp_zone = data['gcp_zone']
      @cluster_prefix = data['cluster_prefix']
    end
  end
end
