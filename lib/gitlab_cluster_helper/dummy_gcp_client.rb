require 'gitlab_cluster_helper'

class GitlabClusterHelper
  class GcpClient
    def create_cluster
      sleep 5

      Cluster.new({
        name: "fake-cluster-#{Time.now.strftime("%Y-%m-%dt%Hh%Mm%Ss")}",
        api_url: 'fake-url',
        ca_certificate: 'fake certificate',
        token: 'fake-token'
      })
    end

    def delete_cluster(cluster_name)
      puts "Deleting: #{cluster_name}"
    end
  end
end
