RSpec.describe GitlabClusterHelper::Shell do
  let(:shell) { described_class.new }
  describe '#run' do
    it 'returns stdout' do
      expect(shell.run(['echo', 'hello', 'world'])).to eq('hello world')
    end

    context 'when the command fails' do
      it 'raises GitlabClusterHelper::Shell::Error' do
        expect{ shell.run(['false']) }.to raise_error(GitlabClusterHelper::Shell::Error)
      end

      it 'includes stderr in exception message' do
        begin
          shell.run(['cat', 'no-such-file'])
        rescue GitlabClusterHelper::Shell::Error => e
          expect(e.message).to include('cat: no-such-file: No such file or directory')
        end
      end

      it 'includes stdout in exception message' do
        begin
          shell.run(['cat', __FILE__, 'no-such-file'])
        rescue GitlabClusterHelper::Shell::Error => e
          expect(e.message).to include('RSpec.describe')
        end
      end
    end
  end
end
