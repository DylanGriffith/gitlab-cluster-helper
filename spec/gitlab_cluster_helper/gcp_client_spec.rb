RSpec.describe GitlabClusterHelper::GcpClient do

  let(:mock_config) do
    instance_double(GitlabClusterHelper::Config,
                    gcp_project: 'my-project',
                    gcp_zone: 'us-central1-a',
                    cluster_prefix: 'my-prefix',
                   )
  end
  let(:mock_shell) { instance_double(GitlabClusterHelper::Shell) }

  let(:client) { described_class.new(config: mock_config, shell: mock_shell) }

  describe '#create_cluster' do
    it 'creates a GKE cluster and fetches and returns details to return a Cluster' do
      expect(Time).to receive(:now).and_return(Time.new(2015, 12, 3))

      # BEGIN: All of the mocking of gcloud and kubectl commands
      expect(mock_shell).to receive(:run)
        .with([
          'gcloud', '--project', 'my-project',
          'container', 'clusters', 'create',
          '--zone', 'us-central1-a',
          'my-prefix-2015-12-03t00h00m00s',
          '--enable-basic-auth',
          '--disk-size', '10GB'
      ]).and_return('')

      expect(mock_shell).to receive(:run)
        .with([
          'gcloud', '--project', 'my-project',
          'container', 'clusters', 'get-credentials',
          '--zone', 'us-central1-a',
          'my-prefix-2015-12-03t00h00m00s',
      ]).and_return('')

      expect(mock_shell).to receive(:run)
        .with([
          'kubectl', 'config', 'view',
          '--minify', '-o', "jsonpath='{.clusters[].cluster.server}'",
      ]).and_return('http://123.123.123.123/')

      expect(mock_shell).to receive(:run)
        .with([
          'gcloud', '--project', 'my-project',
          'container', 'clusters', 'describe',
          '--zone', 'us-central1-a',
          'my-prefix-2015-12-03t00h00m00s',
          '--format', "json(masterAuth.username, masterAuth.password)",
      ]).and_return('{"masterAuth":{"username":"the-user","password":"the-password"}}')

      expect(mock_shell).to receive(:run)
        .with([
          'kubectl', 'config', 'set-credentials',
          'my-prefix-2015-12-03t00h00m00s',
          '--username', 'the-user', '--password', 'the-password',
      ]).and_return('')

      expect(mock_shell).to receive(:run)
        .with(['kubectl', 'create', 'serviceaccount', 'gitlab'])
        .and_return('')

      expect(mock_shell).to receive(:run)
        .with([
          'kubectl', '--user', 'the-user',
          'create', 'clusterrolebinding',
          '--serviceaccount=default:gitlab', '--clusterrole=cluster-admin',
          'gitlab-admin',
      ]).and_return('')

      # Encodes base64 ca.crt=the-cert token=the-token
      secrets_json = '{"items":[{"data":{"ca.crt": "dGhlLWNlcnQ", "token": "dGhlLXRva2Vu"}, "metadata":{"annotations":{"kubernetes.io/service-account.name":"gitlab"}}}]}'
      expect(mock_shell).to receive(:run)
        .with(['kubectl', 'get', 'secrets', '-o', 'json'])
        .and_return(secrets_json)
      # END: All of the mocking of gcloud and kubectl commands

      cluster = client.create_cluster

      expect(cluster).to be_kind_of(GitlabClusterHelper::Cluster)
      expect(cluster.name).to eq('my-prefix-2015-12-03t00h00m00s')
      expect(cluster.api_url).to eq('http://123.123.123.123/')
      expect(cluster.ca_certificate).to eq('the-cert')
      expect(cluster.token).to eq('the-token')
    end
  end

  describe '#delete_cluster' do
    it 'deletes the cluster by name' do
      expect(mock_shell).to receive(:run)
        .with([
          'gcloud', '--project', 'my-project',
          'container', 'clusters', 'delete',
          '--zone', 'us-central1-a',
          'the-cluster',
          '--quiet', '--async',
      ]).and_return('')

      client.delete_cluster('the-cluster')
    end
  end
end
