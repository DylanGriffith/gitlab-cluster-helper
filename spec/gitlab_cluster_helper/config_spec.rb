require 'pathname'

RSpec.describe GitlabClusterHelper::Config do
  before do
    allow(ENV).to receive(:[]).with("GITLAB_CLUSTER_HELPER_CONFIG").and_return(Pathname.new(__dir__).join('sample-config.json').to_s)
  end
  let(:config) { described_class.send(:new) }

  it 'loads the config' do
    expect(config.gcp_project).to eq('sample-project')
    expect(config.gcp_zone).to eq('sample-zone')
    expect(config.cluster_prefix).to eq('sample-prefix')
  end

  context 'when the file does not exist' do
    before do
      allow(ENV).to receive(:[]).with("GITLAB_CLUSTER_HELPER_CONFIG").and_return('non-existant-file.json')
    end

    it 'raises an error' do
      expect { config }
        .to raise_error(GitlabClusterHelper::Config::Error, "Config file does not exist at path: non-existant-file.json")
    end
  end
end
