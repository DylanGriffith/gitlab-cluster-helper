RSpec.describe GitlabClusterHelper::Manager do
  let(:mock_gcp) { instance_double(GitlabClusterHelper::GcpClient, delete_cluster: nil) }
  let(:manager) { described_class.new(gcp_client: mock_gcp) }
  let(:cluster_params) do
    {
      name: 'cluster123',
      api_url: 'http://123.123.123.123/',
      ca_certificate: 'the-cert',
      token: 'the-token',
    }
  end

  describe '#create' do
    it 'returns a cluster' do
      cluster = GitlabClusterHelper::Cluster.new(cluster_params)
      expect(mock_gcp).to receive(:create_cluster)
        .and_return(cluster)

      expect(manager.create).to eq(cluster)
    end
  end

  describe '#cleanup' do
    it 'deletes all the create clusters' do
      cluster1 = GitlabClusterHelper::Cluster.new(cluster_params.merge(name: 'cluster1'))
      cluster2 = GitlabClusterHelper::Cluster.new(cluster_params.merge(name: 'cluster2'))
      cluster3 = GitlabClusterHelper::Cluster.new(cluster_params.merge(name: 'cluster3'))
      cluster4 = GitlabClusterHelper::Cluster.new(cluster_params.merge(name: 'cluster4'))

      allow(mock_gcp).to receive(:create_cluster)
        .and_return(cluster1, cluster2, cluster3, cluster4)

      expect(manager.create).to eq(cluster1)
      expect(manager.create).to eq(cluster2)
      expect(manager.create).to eq(cluster3)

      manager.cleanup

      expect(mock_gcp).to have_received(:delete_cluster).with('cluster1').ordered
      expect(mock_gcp).to have_received(:delete_cluster).with('cluster2').ordered
      expect(mock_gcp).to have_received(:delete_cluster).with('cluster3').ordered
      expect(mock_gcp).to have_received(:delete_cluster).with('cluster4').ordered
    end
  end
end
