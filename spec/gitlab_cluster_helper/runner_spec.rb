require 'tempfile'

RSpec.describe GitlabClusterHelper::Runner do
  let(:readline) { double(:readline, readline: nil) }
  let(:output) { instance_double(IO, puts: nil) }
  let(:manager) { instance_double(GitlabClusterHelper::Manager, create: nil, cleanup: nil) }

  let(:runner) { GitlabClusterHelper::Runner.new(manager: manager, readline_class: readline, output: output) }

  describe '#start' do
    it 'shows the help message' do
      runner.start

      expect(output).to have_received(:puts).with("c) #{GitlabClusterHelper::Messages::CREATE}").ordered
      expect(output).to have_received(:puts).with("q) #{GitlabClusterHelper::Messages::QUIT}").ordered end

    it 'creates a cluster and closes' do
      allow(readline).to receive(:readline).and_return("c", "q", nil)

      allow(manager).to receive(:create).and_return(GitlabClusterHelper::Cluster.new({
        name: 'the-cluster-name',
        api_url: 'the-api-url',
        ca_certificate: 'the-ca-certificate',
        token: 'the-token',
      }))

      all_out = ""
      allow(output).to receive(:puts) do |out|
        all_out << out
      end

      runner.start

      expect(all_out).to include('the-cluster-name')
      expect(all_out).to include('the-api-url')
      expect(all_out).to include('the-ca-certificate')
      expect(all_out).to include('the-token')

      expect(manager).to have_received(:cleanup)
    end
  end
end
