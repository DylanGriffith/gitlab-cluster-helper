# Gitlab Cluster Helper
This is a command line tool you can run locally to help you create and delete clusters easily for doing local development with GitLab features that require a Kubernetes cluster. It will orchestrate Kubernetes cluster and should speed up your development compared to doing all of the setup manually each time you want to try something out.
It's very common to need to create a new cluster every time you need to test
out a new feature and then since it's difficult to re-use a cluster for running
the same test again you will usually find that you need to destroy and recreate
a new cluster which can take time.

The goal of this tool is to speed up this flow by doing the following things
for you:

- [X] Create clusters
- [X] Delete clusters when you are done with them
- [X] Have one spare cluster ready to go at all times (so you don't need to
      wait minutes to get a new cluster)
- [ ] Add the cluster directly to GitLab to save you copy/pasting

## Installation

```
$ gem install gitlab-cluster-helper
```

## Usage

### Setup `gcloud` and `kubectl`

If you have been doing GCP and Kubernetes stuff locally you will probably
already have these setup and can therefor skip these instructions.

If not following the instructions at https://cloud.google.com/sdk/install .

After installing `gcloud` you can install `kubectl` like:

```
$  gcloud components install kubectl
```

Make sure you're also signed in to `gcloud` by running:

```
$ gcloud auth login
```

### Set up the config file

Create a file at `~/.gitlab-cluster-helper-config.json` with the following content:

```json
{
  "gcp_project": "Enter the GCP project ID here",
  "gcp_zone": "The zone where clusters are created (eg. us-central1-a)",
  "cluster_prefix": "The prefix to use for naming your clusters (eg. dgriffith)"
}
```

### Run the application

```
$ gitlab-cluster-helper
c) Create new cluster and show credentials
q) Quit application and clean up any clusters that were created
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run
`rake spec` to run the tests. You can also run `bin/console` for an interactive
prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To
release a new version, update the version number in `version.rb`, and then run
`bundle exec rake release`, which will create a git tag for the version, push
git commits and tags, and push the `.gem` file to
[rubygems.org](https://rubygems.org).

### Running mocked GCP locally

When testing the application locally you may prefer to not really be creating
GCP clusters. So you can easily used a mocked version that sleeps for 5s before
creating a fake cluster:

```bash
$ ruby -I lib -r gitlab_cluster_helper/dummy_gcp_client exe/gitlab-cluster-helper
```

The 5s delay helps with testing out the behaviour where it starts creating the
next cluster before you've asked for it.

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/DylanGriffith/gitlab-cluster-helper/issues .

## License

The gem is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).
